createOrderedClusters <- function(x, num.clusters, global.min = -Inf, global.max = Inf, ...) {
    clusters <- getKMeansPP(x=x, num.centres = num.clusters, ...)

    cluster.info <- data.frame(OriginalNum = 1:num.clusters, Centre = as.vector(clusters$centers), Size = clusters$size)
    cluster.info <- cluster.info[order(cluster.info$Centre),]
    cluster.info$Num <- 1:num.clusters

    clustered.x <- data.frame(X = x, OriginalNum = clusters$cluster)



    # clustered.x <- lonelyr::mergeAndKeepOrder(x = clustered.x, y = cluster.info, by='OriginalNum', all.x = TRUE)

    cluster.mins <- aggregate(clustered.x$X, by=list(clustered.x$OriginalNum), FUN=min)
    names(cluster.mins) <- c('OriginalNum', 'Min')
    cluster.maxs <- aggregate(clustered.x$X, by=list(clustered.x$OriginalNum), FUN=max)
    names(cluster.maxs) <- c('OriginalNum', 'Max')
    cluster.lims <- merge(cluster.mins, cluster.maxs, by='OriginalNum'); remove(cluster.maxs, cluster.mins)

    cluster.info <- lonelyr::mergeAndKeepOrder(x = cluster.info, y = cluster.lims, by='OriginalNum')
    cluster.info$PrevNum <- cluster.info$Num-1
    cluster.info$NextNum <- cluster.info$Num+1

    cluster.info2 <- lonelyr::mergeAndRenameCols(x = cluster.info, y = cluster.info[,c('Num','Max')],
                                                 by.x = 'PrevNum', by.y = 'Num', all.x = TRUE, y.name = 'Prev', prefix = T)
    cluster.info2 <- lonelyr::mergeAndRenameCols(x = cluster.info2, y = cluster.info[,c('Num','Min')],
                                                 by.x = 'NextNum', by.y = 'Num', all.x = TRUE, y.name = 'Next', prefix = T)

    cluster.info <- cluster.info2; remove(cluster.info2)


    cluster.info$LowerBound <- ifelse(is.na(cluster.info$PrevMax), yes = global.min, no=(cluster.info$Min+cluster.info$PrevMax)/2.0)
    cluster.info$UpperBound <- ifelse(is.na(cluster.info$NextMin), yes = global.max, no=(cluster.info$Max+cluster.info$NextMin)/2.0)



    result <- lonelyr::mergeAndKeepOrder(x = clustered.x, y = cluster.info, by = 'OriginalNum', all.x = TRUE)
    result$Cluster <- result$Num

    result <- result[,c('X', 'Cluster', 'Centre', 'Size', 'Min', 'Max', 'LowerBound', 'UpperBound')]
    return(result)
}

# Example
# x <- runif(n = 10, min = 1, max = 10)
# clusters <- createOrderedClusters(x = x, num.clusters = 3, global.min = 0, global.max = 13)
